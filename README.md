# Quick Setup

Documentation and tools to quickly get communication and collaboration tools set up on a server.

## Overview

This will initially consist of documentation / scripts to get
Ghost, Discourse, and OnlyOffice running, each in a container along with dependencies,
each mapped to a subdomain with proper nginx config,
each served over HTTPS via letsencrypt.
